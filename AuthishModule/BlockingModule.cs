﻿using System;
using System.Web;

namespace AuthishModule
{
    public class BlockingModule : IHttpModule
    {

        public void Init(HttpApplication context)
        {
            context.PreRequestHandlerExecute += context_PostAcquireRequestState;
        }

        private void context_PostAcquireRequestState(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;

            if (app.Request.RawUrl.IndexOf("api", StringComparison.InvariantCultureIgnoreCase) != -1 ||
                CookieHelper.IsAuthenticated(app.Context) ||
                ValidationService.PasswordIsCorrect(app.Context.Request.Headers["Authish"]) ||
                ValidationService.PathIsWhitelisted(app.Context.Request.Path))
            {
                return;
            }

            app.Context.Handler = new AuthishHandler();
        }

        public void Dispose()
        {
            
        }
    }
}
