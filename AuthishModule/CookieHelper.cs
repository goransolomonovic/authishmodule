﻿using System;
using System.Web;

namespace AuthishModule
{
    public class CookieHelper
    {
        private const string CookieName = "AuthishModule_Authenticated";
        public static bool IsAuthenticated(HttpContext context)
        {
            return context.Request.Cookies[CookieName] != null;
        }

        public static void SetAuthenticated(HttpContext context)
        {
            context.Response.SetCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddMonths(1) });
        }
    }
}
